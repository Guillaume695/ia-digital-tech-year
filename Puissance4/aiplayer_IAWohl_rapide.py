# -*-coding:Utf-8 -*

from player import Player
import random
import copy

class AIPlayer(Player):

    def __init__(self):
        super(AIPlayer, self).__init__()
        self.height = 0
        self.width = 0
        self.name = "IA Wohl rapide"

    def getColumn(self, model):
        """Methode qui retourne la colonne à jouer"""
        # Mask du plateau pour savoir quels pions ont été parcourus
        self.width = len(model)
        self.height = len(model[0])
        while True:
            m, col = self.minimax(model, 7, self.color)
            """index = random.randint(0,6)
            if model[index][-1]==0:
                return index"""
            return col

    def heuristic(self, model):
        return (self.coupsAvantVictoire(model, -self.color) - self.coupsAvantVictoire(model, self.color))**3

    def coupsAvantVictoire(self, model, player_id):
        rep = []
        self.width = len(model)
        self.height = len(model[0])
        for i in range(len(model[0])):
            for j in range(len(model)):
                if (self.inGame(i-3, j)):
                    alignement = [ model[j][i-k] for k in range(4) ]
                    if (self.possibleAlignement(alignement, player_id)):
                        rep.append(self.intAlignement((i,j), (i-3, j), alignement, model, player_id))
                if (self.inGame(i, j+3)):
                    alignement = [ model[j+k][i] for k in range(4) ]
                    if (self.possibleAlignement(alignement, player_id)):
                        rep.append(self.intAlignement((i,j), (i,j+3), alignement, model, player_id))
                if (self.inGame(i+3, j+3)):
                    alignement = [ model[j+k][i+k] for k in range(4) ]
                    if (self.possibleAlignement(alignement, player_id)):
                        rep.append(self.intAlignement((i,j), (i+3,j+3), alignement, model, player_id))
                if (self.inGame(i-3, j+3)):
                    alignement = [ model[j+k][i-k] for k in range(4) ]
                    if (self.possibleAlignement(alignement, player_id)):
                        rep.append(self.intAlignement((i,j), (i-3, j+3), alignement, model, player_id))

        return min(rep)

    def inGame(self, i, j):
        if (i < self.height and j < self.width):
            if (i >= 0 and j >= 0):
                return True
            else:
                return False
        else:
            return False

    def possibleAlignement(self, alignement, player_id):
        if (player_id == 1):
            other_id = -1
        else:
            other_id = 1
        if (alignement.count(player_id)):
            if (alignement.count(other_id)):
                return False
            else:
                return True
        else:
            if (alignement.count(0) == 4):
                return True
            else:
                return False

    def intAlignement(self, startPoint, endPoint, alignement, model, player_id):
        # Nombre de cases vides sur l'alignement
        rep = 4 - (alignement.count(player_id))
        # Nombre de cases vides pour accéder à l'alignement
        # Si on est sur une colonne, on s'arrête là
        if (startPoint[1] == endPoint[1]):
            j = startPoint[1]
            for k in range(endPoint[0]):
                if (model[j][k] == 0):
                    rep += 1
            return rep
        # Sinon, il faut compter le nombre de cases vides jusqu'à chaque élément de l'alignement
        if (startPoint[0] == endPoint[0]):
            i = startPoint[0]
            for j in range(startPoint[1], endPoint[1]+1):
                for k in range(i):
                    if (model[j][k] == 0):
                        rep += 1
        # Si on monte en diagonale
        if (endPoint[0] > startPoint[0]):
            for k in range(4):
                for l in range(startPoint[0]+k):
                    if (model[startPoint[1]+k][l] == 0):
                        rep += 1
        # Si on descend en diagonale
        if (endPoint[0] < startPoint[0]):
            for k in range(4):
                for l in range(startPoint[0]-k):
                    if (model[startPoint[1]+k][l] == 0):
                        rep += 1
        return rep

    def modelPossible(self, model, couleur):
        list_model = []
        for i in range(0, len(model)):
            copy_model = copy.deepcopy(model)
            row = 0
            while row < len(model[i]) and model[i][row] != 0: row += 1
            if row < len(model[i]):
                copy_model[i][row] = couleur
                list_model.append((copy_model, i))
        return list_model

    def minimax(self, model_original, profondeur, couleur):
        model_possible = self.modelPossible(model_original, couleur)
        list_score = []
        list_colonne = []
        for i in range (0, len(model_possible)):
            model, col = model_possible[i]
            if self.coupsAvantVictoire(model, self.color) == 0:
                score = 10000000
            elif self.coupsAvantVictoire(model, -self.color) == 0:
                score = -10000000
            else:
                if profondeur != 1:
                    score, _ = self.minimax(model, profondeur - 1, -couleur)
                else:
                    score = self.heuristic(model)
            list_score.append(score)
            list_colonne.append(col)
        if couleur == self.color:
            m = max(list_score)
        else:
            m = min(list_score)
        return m, list_colonne[list_score.index(m)]


    def getColumn(self, model):
        """Methode qui retourne la colonne à jouer"""
        while True:
            m, col = self.minimax(model, 4, self.color)
            """index = random.randint(0,6)
            if model[index][-1]==0:
                return index"""
            return col
        #on ne devrait pas arriver ici
