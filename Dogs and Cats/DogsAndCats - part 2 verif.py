# -*-coding:Utf-8 -*
# DTY 2017 - Intelligence Artificielle
# ./DogsAndCats - part 2 test.py

# ------------------------------ Testing model ----------------------------------------

from skimage import data, io
from matplotlib.pyplot import imshow
import matplotlib.pyplot as plt
from keras.preprocessing import image
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy as np
import os
import cv2

# load json and create model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

cat_path = 'test1/10002.jpg'
dog_path = 'test1/10000.jpg'
img_path = 'alex.jpeg'

ima=io.imread(img_path)
imshow(ima)

# image loading and transformation to keras
img = cv2.imread(img_path)
img = cv2.resize(img,(150,150))
img_keras = np.reshape(img,[1,150,150,3])

# evaluate the model
prediction = loaded_model.predict_classes(img_keras)

if (prediction[0][0] == 1):
    print("C'est un chien!!!!")
elif (prediction[0][0] == 0):
    print("C'est un chat!!!!")

plt.show()
