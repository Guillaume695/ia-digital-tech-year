# -*-coding:Utf-8 -*
# DTY 2017 - Intelligence Artificielle
# ./DogsAndCats - part 2.py

# -------------------------- Data preparation and loading ------------------------------------

# without augmentation, only rescaling
from keras.preprocessing.image import ImageDataGenerator

# definition of the number of samples propagated through the network at each step
batch_size = 16

# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = 'train'
validation_data_dir = 'valid'

# create and configure an ImageDataGenerator for the training data
# With augmentation of the training data using rotation, horizontal and vertical shift, shearing tranformation, zooming

train_datagen =  ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

# create and configure an ImageDataGenerator for the validation data with only rescaling to 0..1
test_datagen =  ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,  # the target directory
        target_size=(img_width, img_height),  # all images will be resized to 150x150
        batch_size=batch_size,
        class_mode='binary')

valid_generator = test_datagen.flow_from_directory(
        validation_data_dir,  # the target directory
        target_size=(img_width, img_height),  # all images will be resized to 150x150
        batch_size=batch_size,
        class_mode='binary')


# -------------------------- Build a model from scratch ------------------------------------

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense

model = Sequential()

# First convolutional layer
model.add(Conv2D(10, (5,5), activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(pool_size=4, strides=None, padding='valid'))

# Second  convolutional layer
model.add(Conv2D(10, (5,5), activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(pool_size=4, strides=None, padding='valid'))

# Third convolutional layer
model.add(Conv2D(10, (5,5), activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(pool_size=4, strides=None, padding='valid'))

# Adding of two fully-connected layers
model.add(Flatten())
model.add(Dense(32, activation='relu'))
model.add(Dense(16, activation='relu'))

# single unit and sigmoid activation, which is perfect for a binary classification.

model.add(Dense(1))
model.add(Activation('sigmoid'))

# Use of the binary_crossentropy loss to train our model, of the rmsprop optimizer and the accuracy metrics

model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['accuracy'])

print(model.summary())


# ------------------------------ Training a model ----------------------------------------

# model training
model.fit_generator(train_generator, steps_per_epoch=23000//batch_size, epochs=10, validation_data=valid_generator, validation_steps=23000//batch_size)

# serialize model to JSON
model_json = model.to_json()
with open("model_full.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model_full.h5")
print("Saved model to disk")
