# -*-coding:Utf-8 -*
# DTY 2017 - Intelligence Artificielle
# ./DogsAndCats - part 1.py

# -------------------------- Visual Vocabulary Building ------------------------------------
import cv2
import imutils
import numpy as np
import os
from paths import *
from scipy.cluster.vq import *

# Here you have to extract SIFT features on all images of the training set and to make a k-means clustering to build the visual vocabulary.

# Paths to trainning datasets
catsTrainPaths = list(list_images('med_sample/train/cats'))
dogsTrainPaths = list(list_images('med_sample/train/dogs'))


# Get all the path to the images and save them in a list
# image_paths and the corresponding label in image_classes
image_paths = catsTrainPaths+dogsTrainPaths
image_classes = [["cat"]*len(catsTrainPaths)]+[["dog"]*len(dogsTrainPaths)]

# Create feature extraction and keypoint detector objects using opencv or other librairies such as pyvlfeat
sift_features = cv2.xfeatures2d.SIFT_create()

# List where all the descriptors are stored
des_list = []

for pet in image_paths:
    img_temp = cv2.imread(pet)
    gray = cv2.cvtColor(img_temp, cv2.COLOR_BGR2GRAY)
    kp, des = sift_features.detectAndCompute(gray,None)
    des_list.append([pet, des])

# Stack all the descriptors vertically in a numpy array
descriptors = des_list[0][1]
for image_path, descriptor in des_list[1:]:
    descriptors = np.vstack((descriptors, descriptor))

# Perform k-means clustering
k = 100
voc, variance = kmeans(descriptors, k, 1)
