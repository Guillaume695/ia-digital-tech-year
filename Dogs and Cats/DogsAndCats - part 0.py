# -*-coding:Utf-8 -*
# DTY 2017 - Intelligence Artificielle
# ./DogsAndCats - part 0.py

# -------------------------- Image representation ------------------------------------
import skimage.data
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os.path

# First representation - Raw data
def image_to_feature_vector(image, size=(32, 32)):
    if not(os.path.isfile(image)):
        print("File not found ...")
        return 0
    # Open image
    img_raw = skimage.data.imread(image)
    # resize the image to a fixed size
    img_resized = skimage.transform.resize(img_raw, size, mode="reflect")
    # then flatten the image into a list of raw pixel intensities
    img_flattened = np.ndarray.flatten(img_resized)
    return img_flattened

# image_to_feature_vector("train/cats/cat.394.jpg")

# Second representation - color histogram in HSV color space
def build_HSV_color_histogram_vector(image,bins=(8, 8, 8)):
    if not(os.path.isfile(image)):
        print("File not found ...")
        return 0
    # extract a 3D color histogram from the HSV color space using the supplied number of `bins` per channel and return it as a feature vector
    img_raw = cv2.imread(image)
    hsv = cv2.cvtColor(img_raw, cv2.COLOR_BGR2HSV)

    res = np.array([])
    for i in range(3):
        max_value = int(np.max(hsv[:,:,i]))
        histr = cv2.calcHist([hsv],[i],None,[bins[i]],[0,max_value])
        res = np.concatenate((res, histr[:,0]))

    # plt.bar(range(24), res)
    # plt.show()

    return res

# build_HSV_color_histogram_vector("train/cats/cat.394.jpg")


# ----------------------- Dataset prepatation and feature extraction --------------------------
from paths import *

# Paths to trainning datasets
catsTrainPaths = list(list_images('med_sample/train/cats'))
dogsTrainPaths = list(list_images('med_sample/train/dogs'))
n_pets = len(catsTrainPaths+dogsTrainPaths)

# initialize the raw pixel intensities matrix, the features matrix and labels list
print("Loading "+str(n_pets)+" pictures ...")
rawImages_features = np.array([image_to_feature_vector(elem) for elem in catsTrainPaths+dogsTrainPaths])
histogram_features = np.array([build_HSV_color_histogram_vector(elem) for elem in catsTrainPaths+dogsTrainPaths])
labels = np.array(["cat"]*len(catsTrainPaths)+["dog"]*len(dogsTrainPaths))


# ------------------- Dataset splitting into training and validation dataset ------------------
from sklearn.model_selection import train_test_split

X_raw_train, X_raw_test, y_raw_train, y_raw_test = train_test_split(rawImages_features, labels, test_size = 0.25, random_state = 0)
X_hsv_train, X_hsv_test, y_hsv_train, y_hsv_test = train_test_split(histogram_features, labels, test_size = 0.25, random_state = 0)


# ---------------- Classification using the K-Nearest Neighbor (KNN) classifier ---------------
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report

# Number of neighbors used for KNN classifier
k_neighbor = 9

# Classifiers
raw_clf = KNeighborsClassifier(n_neighbors=k_neighbor)
hsv_clf = KNeighborsClassifier(n_neighbors=k_neighbor)

# Fit to datasets
print("Training ...")
raw_clf.fit(X_raw_train, y_raw_train)
hsv_clf.fit(X_hsv_train, y_hsv_train)

# Predictions
print("Predicting ...")
y_raw_pred = raw_clf.predict(X_raw_test)
y_hsv_pred = hsv_clf.predict(X_hsv_test)

# Raw and hsv accuracy
n_raw_tests = len(y_raw_test)
n_hsv_tests = len(y_hsv_test)
raw_accuracy = sum([float(y_raw_test[i] == y_raw_pred[i]) for i in range(n_raw_tests)])/n_raw_tests
hsv_accuracy = sum([float(y_hsv_test[i] == y_hsv_pred[i]) for i in range(n_hsv_tests)])/n_hsv_tests

print("Raw pixel representation accuracy: "+str(raw_accuracy))
print("Color histogram representation accuracy: "+str(hsv_accuracy))


# ------------------------ Validation sets for Hyperparameter tuning -------------------------
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
import time

print
print("Building training/testing split...")
X_raw_train2, X_raw_valid, y_raw_train2, y_raw_valid = train_test_split(X_raw_train, y_raw_train, test_size = 0.25, random_state = 0)
X_hsv_train2, X_hsv_valid, y_hsv_train2, y_hsv_valid = train_test_split(X_hsv_train, y_hsv_train, test_size = 0.25, random_state = 0)

# Parameter values for grid search
params = {"n_neighbors": np.arange(1, 29, 1),"metric": ["euclidean", "cityblock"]}

# tune the hyperparameters via a cross-validated grid search of the KNN classifier on raw features
print("Tuning hyperparameters via grid search on raw features")
raw_clf_cross = GridSearchCV(KNeighborsClassifier(), params)
start = time.time()
# evaluate the best grid searched model on the testing data
raw_clf_cross.fit(X_raw_train, y_raw_train)
print("Done in "+str(time.time()-start)+" secondes. Best parameters are: ")
print(raw_clf_cross.best_estimator_)
print

# tune the hyperparameters via a cross-validated randomized search of the KNN classifier on raw features
print("Tuning hyperparameters via randomized search on raw features")
raw_clf_rand = RandomizedSearchCV(KNeighborsClassifier(), params)
start = time.time()
# evaluate the best grid searched model on the testing data
raw_clf_rand.fit(X_raw_train, y_raw_train)
print("Done in "+str(time.time()-start)+" secondes. Best parameters are: ")
print(raw_clf_rand.best_estimator_)
print

# tune the hyperparameters via a cross-validated grid search of the KNN classifier on histo features
print("Tuning hyperparameters via grid search on histo features")
hsv_clf_cross = GridSearchCV(KNeighborsClassifier(), params)
start = time.time()
# evaluate the best grid searched model on the testing data
hsv_clf_cross.fit(X_hsv_train, y_hsv_train)
print("Done in "+str(time.time()-start)+" secondes. Best parameters are: ")
print(hsv_clf_cross.best_estimator_)
print

# tune the hyperparameters via a cross-validated randomized search of the KNN classifier on raw features

print("Tuning hyperparameters via randomized search on histo features")
hsv_clf_rand = RandomizedSearchCV(KNeighborsClassifier(), params)
start = time.time()
# evaluate the best grid searched model on the testing data
hsv_clf_rand.fit(X_hsv_train, y_hsv_train)
print("Done in "+str(time.time()-start)+" secondes. Best parameters are: ")
print(hsv_clf_rand.best_estimator_)
print


# ----------------------------------- Cross-validation --------------------------------------
from sklearn.model_selection import cross_val_score


# creating odd list of K for KNN
params = {"n_neighbors": np.arange(1, 29, 2)}

# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation on the KNN classifier with raw features
print(" 10-fold cross validation on the KNN classifier with raw features")

for k in params["n_neighbors"]:
    cv_scores.append(sum(cross_val_score(KNeighborsClassifier(k), X_raw_train, y_raw_train, cv=10))/10)

print("Plotting the misclassification error versus k")

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k = MSE.index(min(MSE))
print("The optimal number of neighbors is %d" % optimal_k)

# plot misclassification error vs k
plt.plot(params["n_neighbors"], MSE)
plt.xlabel('Number of Neighbors K')
plt.ylabel('Misclassification Error')
plt.show()



# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation on the KNN classifier with raw features
print(" 10-fold cross validation on the KNN classifier with hist features")

for k in params["n_neighbors"]:
    cv_scores.append(sum(cross_val_score(KNeighborsClassifier(k), X_hsv_train, y_hsv_train, cv=10))/10)

print("Plotting the misclassification error versus k")

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k = MSE.index(min(MSE))
print("The optimal number of neighbors is %d" % optimal_k)

# plot misclassification error vs k
plt.plot(params["n_neighbors"], MSE)
plt.xlabel('Number of Neighbors K')
plt.ylabel('Misclassification Error')
plt.show()
