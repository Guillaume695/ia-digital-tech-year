# -*-coding:Utf-8 -*
# Intelligence Artificielle - CSP

# -------------------- Water Retention Magic Squares --------------------
from Numberjack import *

def model_water(N):
    board = [[Variable(range(1, N*N+1)) for i in range(N)] for j in range(N)]
    const = []

    list_cells = []
    for k in range(N):
        for l in range(N):
            list_cells.append(board[k][l])
    const.append(AllDiff(list_cells))

    model = Model(const)
    return (board, model)

def print_sudoku(board):
    separator = '+-----'*len(board)+'+'
    print(separator)
    for ligne in board:
        str_print = ''
        j = 0
        for elem in ligne:
            if (elem.get_value() < 100):
                if (elem.get_value() < 10):
                    str_print += '| 00'+str(elem.get_value()) + ' '
                else:
                    str_print += '| 0'+str(elem.get_value()) + ' '
            else:
                str_print += '| '+str(elem.get_value()) + ' '
        str_print += '|'
        print(str_print)
        print(separator)

def solve_sudoku(param):
    (board,model) = model_water(param['N'])
    solver = model.load(param['solver'])
    solver.solve()
    print_sudoku(board)
    print('Nodes:', solver.getNodes(), ' Time:', solver.getTime())

parameters = {'solver':'Mistral', 'N':10}
solve_sudoku(parameters)
