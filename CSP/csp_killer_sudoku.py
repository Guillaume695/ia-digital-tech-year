# -*-coding:Utf-8 -*
# Intelligence Artificielle - CSP

# -------------------- solving the N-queens problem with Numberjack. --------------------
from Numberjack import *

problem1 = [
      [3, [[1, 1], [1, 2]]],
      [15, [[1, 3], [1, 4], [1, 5]]],
      [22, [[1, 6], [2, 5], [2, 6], [3, 5]]],
      [4, [[1, 7], [2, 7]]],
      [16, [[1, 8], [2, 8]]],
      [15, [[1, 9], [2, 9], [3, 9], [4, 9]]],
      [25, [[2, 1], [2, 2], [3, 1], [3, 2]]],
      [17, [[2, 3], [2, 4]]],
      [9, [[3, 3], [3, 4], [4, 4]]],
      [8, [[3, 6], [4, 6], [5, 6]]],
      [20, [[3, 7], [3, 8], [4, 7]]],
      [6, [[4, 1], [5, 1]]],
      [14, [[4, 2], [4, 3]]],
      [17, [[4, 5], [5, 5], [6, 5]]],
      [17, [[4, 8], [5, 7], [5, 8]]],
      [13, [[5, 2], [5, 3], [6, 2]]],
      [20, [[5, 4], [6, 4], [7, 4]]],
      [12, [[5, 9], [6, 9]]],
      [27, [[6, 1], [7, 1], [8, 1], [9, 1]]],
      [6, [[6, 3], [7, 2], [7, 3]]],
      [20, [[6, 6], [7, 6], [7, 7]]],
      [6, [[6, 7], [6, 8]]],
      [10, [[7, 5], [8, 4], [8, 5], [9, 4]]],
      [14, [[7, 8], [7, 9], [8, 8], [8, 9]]],
      [8, [[8, 2], [9, 2]]],
      [16, [[8, 3], [9, 3]]],
      [15, [[8, 6], [8, 7]]],
      [13, [[9, 5], [9, 6], [9, 7]]],
      [17, [[9, 8], [9, 9]]]]

def convert_index(l):
    for x in l:
        for y in x[1]:
            for z in range(len(y)):
                y[z] -= 1

convert_index(problem1)

def model_sudoku(N):
    board = [[Variable([k for k in range(1, N+1)]) for i in range(N)] for j in range(N)]
    const = []
    # Sudoku killer
    for const_temp in problem1:
        const.append( Sum( [board[e[0]][e[1]] for e in const_temp[1]] ) == const_temp[0])
    # Sudoku
    for i in range(N):
        # Ligne
        const.append(AllDiff([board[i][j] for j in range(N)]))
        # Colonne
        const.append(AllDiff([board[j][i] for j in range(N)]))
    # Carré
    for i in range(3):
        for j in range(3):
            list_cells = []
            for k in range(3):
                for l in range(3):
                    list_cells.append(board[i*3+k][j*3+l])
            const.append(AllDiff(list_cells))

    model = Model(const)
    return (board, model)

def print_sudoku(board):
    separator = '+---'*len(board)+'+'
    print(separator)
    for ligne in board:
        str_print = ''
        j = 0
        for elem in ligne:
            str_print += '| '+str(elem.get_value()) + ' '
        str_print += '|'
        print(str_print)
        print(separator)

def solve_sudoku(param):
    (board,model) = model_sudoku(param['N'])
    solver = model.load(param['solver'])
    solver.solve()
    print_sudoku(board)
    print('Nodes:', solver.getNodes(), ' Time:', solver.getTime())

parameters = {'solver':'Mistral', 'N':9}
solve_sudoku(parameters)
