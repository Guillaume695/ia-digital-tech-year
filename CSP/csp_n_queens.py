# -*-coding:Utf-8 -*
# Intelligence Artificielle - CSP

# -------------------- solving the N-queens problem with Numberjack. --------------------
from Numberjack import *

# --------- First method -----------
def model_queens_1(N):
    queens = [Variable(N) for i in range(N)]
    model  = Model(
        AllDiff( queens ),
        AllDiff( [queens[i] + i for i in range(N)] ),
        AllDiff( [queens[i] - i for i in range(N)] )
        )
    return (queens,model)

def solve_queens(param):
    (queens,model) = model_queens_1(param['N'])
    solver = model.load(param['solver'])
    solver.solve()
    print_chessboard(queens)
    print('Nodes:', solver.getNodes(), ' Time:', solver.getTime())

def print_chessboard(queens):
    separator = '+---'*len(queens)+'+'
    for queen in queens:
        print(separator)
        print( '|   '*queen.get_value()+'| Q |'+'   |'*(len(queens)-1-queen.get_value()))
        print(separator)

# --------- Second method -----------
def model_queens_2(N):
    board = [[Variable([0,1]) for i in range(N)] for j in range(N)]
    const = []
    for i in range(N):
        const.append( Sum( [board[i][j] for j in range(N)] ) == 1)
        const.append( Sum( [board[j][i] for j in range(N)] ) == 1 )
        const.append( Sum( [board[i+k][k] for k in range(N-i)] ) <= 1 )
        const.append( Sum( [board[k][k+i] for k in range(N-i)] ) <= 1 )
        const.append( Sum( [board[k][N - (k + i) - 1] for k in range(N-i)] ) <= 1 )
        const.append( Sum( [board[N - k - 1][k + i] for k in range(N-i)] ) <= 1 )
    model = Model(const)
    return (board, model)

def solve_queens2(param):
    (queens,model) = model_queens_2(param['N'])
    solver = model.load(param['solver'])
    solver.solve()
    print_chessboard2(queens)
    print('Nodes:', solver.getNodes(), ' Time:', solver.getTime())


def print_chessboard2(queens):
    separator = '+---'*len(queens)+'+'
    print(separator)
    for queen_ligne in queens:
        str_print = ''
        j = 0
        for queen in queen_ligne:
            if (queen.get_value() == 1):
                str_print += '|   '*j+'| Q |'+'   |'*(len(queen_ligne)-1-j)
            j += 1
        print(str_print)
        print(separator)

parameters = {'solver':'Mistral', 'N':5}
solve_queens(parameters)
solve_queens2(parameters)
